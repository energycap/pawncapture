﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PawnCAPture
{
    public partial class PawnCAPtureDatabaseContext : DbContext
    {
        public PawnCAPtureDatabaseContext()
        {
        }

        public PawnCAPtureDatabaseContext(DbContextOptions<PawnCAPtureDatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Game> Games { get; set; } = null!;
        public virtual DbSet<Move> Moves { get; set; } = null!;
        public virtual DbSet<Player> Players { get; set; } = null!;
        public virtual DbSet<Team> Teams { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>(entity =>
            {
                entity.ToTable("Game");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.EndDate).HasColumnName("endDate");

                entity.Property(e => e.StartDate).HasColumnName("startDate");
            });

            modelBuilder.Entity<Move>(entity =>
            {
                entity.ToTable("Move");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Fen)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("fen");

                entity.Property(e => e.Game).HasColumnName("game");

                entity.Property(e => e.Player).HasColumnName("player");

                entity.Property(e => e.Timestamp).HasColumnName("timestamp");

                entity.HasOne(d => d.GameNavigation)
                    .WithMany(p => p.Moves)
                    .HasForeignKey(d => d.Game)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Move_Game");

                entity.HasOne(d => d.PlayerNavigation)
                    .WithMany(p => p.Moves)
                    .HasForeignKey(d => d.Player)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Move_Player");
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.ToTable("Player");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ExternalUserId).HasColumnName("externalUserId");

                entity.Property(e => e.Team).HasColumnName("team");

                entity.Property(e => e.Name).HasColumnName("name");
             
                entity.HasOne(d => d.TeamNavigation)
                    .WithMany(p => p.Players)
                    .HasForeignKey(d => d.Team)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Player_Team");
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.ToTable("Team");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Label)
                    .HasMaxLength(10)
                    .HasColumnName("label")
                    .IsFixedLength();

                entity.HasData(
                    new Team
                    {
                        Id = 1,
                        Label = "White"
                    },
                    new Team
                    {
                        Id = 2,
                        Label = "Black"
                    });
            });



            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
