﻿using System;
using System.Collections.Generic;

namespace PawnCAPture
{
    public partial class Game
    {
        public Game()
        {
            Moves = new HashSet<Move>();
        }

        public int Id { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }

        public virtual ICollection<Move> Moves { get; set; }
    }
}
