﻿namespace PawnCAPture.Data
{
    public class BoardStatus
    {
        public string Turn { get; set; }
        public bool Check { get; set; }
        public bool Checkmate { get; set; }
        public bool Draw { get; set; }
        public bool Stalemate { get; set; }
    }

  
}
