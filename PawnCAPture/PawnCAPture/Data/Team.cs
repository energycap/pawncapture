﻿using System;
using System.Collections.Generic;

namespace PawnCAPture
{
    public partial class Team
    {
        public Team()
        {
            Players = new HashSet<Player>();
        }

        public int Id { get; set; }
        public string Label { get; set; } = null!;

        public virtual ICollection<Player> Players { get; set; }
    }
}
