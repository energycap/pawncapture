﻿using System;
using System.Collections.Generic;

namespace PawnCAPture
{
    public partial class Player
    {
        public Player()
        {
            Moves = new HashSet<Move>();
        }

        public int Id { get; set; }
        public int Team { get; set; }
        public string ExternalUserId { get; set; }
        public string? Name { get; set; }

        public virtual Team TeamNavigation { get; set; } = null!;
        public virtual ICollection<Move> Moves { get; set; }
    }
}
