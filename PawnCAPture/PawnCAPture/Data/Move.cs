﻿using System;
using System.Collections.Generic;

namespace PawnCAPture
{
    public partial class Move
    {
        public int Id { get; set; }
        public string Fen { get; set; } = null!;
        public DateTimeOffset Timestamp { get; set; }
        public int Player { get; set; }
        public int Game { get; set; }

        public virtual Game GameNavigation { get; set; } = null!;
        public virtual Player PlayerNavigation { get; set; } = null!;
    }
}
