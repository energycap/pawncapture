﻿using Microsoft.AspNetCore.SignalR;

namespace PawnCAPture
{
    public class PawnCAPtureHub: Hub
    {
        public const string HubUrl = "/chess";

        public async Task BoardUpdated(string currentFen)
        {
            await Clients.Others.SendAsync("BoardUpdated", currentFen);
        }

        public async Task GameCreated()
        {
            await Clients.Others.SendAsync("GameCreated");
        }

        public override Task OnConnectedAsync()
        {
            Console.WriteLine($"{Context.ConnectionId} connected");
            return base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception e)
        {
            Console.WriteLine($"Disconnected {e?.Message} {Context.ConnectionId}");
            await base.OnDisconnectedAsync(e);
        }
    }
}
