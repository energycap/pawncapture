﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PawnCAPture.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Game",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    startDate = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                    endDate = table.Column<DateTimeOffset>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Game", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Team",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    label = table.Column<string>(type: "TEXT", fixedLength: true, maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Team", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Player",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    team = table.Column<int>(type: "INTEGER", nullable: false),
                    externalUserId = table.Column<string>(type: "TEXT", nullable: false),
                    name = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Player", x => x.id);
                    table.ForeignKey(
                        name: "FK_Player_Team",
                        column: x => x.team,
                        principalTable: "Team",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "Move",
                columns: table => new
                {
                    id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    fen = table.Column<string>(type: "TEXT", unicode: false, maxLength: 250, nullable: false),
                    timestamp = table.Column<DateTimeOffset>(type: "TEXT", nullable: false),
                    player = table.Column<int>(type: "INTEGER", nullable: false),
                    game = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Move", x => x.id);
                    table.ForeignKey(
                        name: "FK_Move_Game",
                        column: x => x.game,
                        principalTable: "Game",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_Move_Player",
                        column: x => x.player,
                        principalTable: "Player",
                        principalColumn: "id");
                });

            migrationBuilder.InsertData(
                table: "Team",
                columns: new[] { "id", "label" },
                values: new object[] { 1, "White" });

            migrationBuilder.InsertData(
                table: "Team",
                columns: new[] { "id", "label" },
                values: new object[] { 2, "Black" });

            migrationBuilder.CreateIndex(
                name: "IX_Move_game",
                table: "Move",
                column: "game");

            migrationBuilder.CreateIndex(
                name: "IX_Move_player",
                table: "Move",
                column: "player");

            migrationBuilder.CreateIndex(
                name: "IX_Player_team",
                table: "Player",
                column: "team");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Move");

            migrationBuilder.DropTable(
                name: "Game");

            migrationBuilder.DropTable(
                name: "Player");

            migrationBuilder.DropTable(
                name: "Team");
        }
    }
}
