﻿


function renderBoard(dotNetHelper, position, _team) {
    var board,
        // Chess library does not deal well with null as an argument
        game = new Chess(position ?? undefined),
        whiteSquareGrey = '#a9a9a9',
        blackSquareGrey = '#696969',
        dotNetHelper = dotNetHelper,
        team = _team;

    function removeGreySquares() {
        $('#board .square-55d63').css('background', '')
    }

    function greySquare(square) {
        var $square = $('#board .square-' + square)

        var background = whiteSquareGrey
        if ($square.hasClass('black-3c85d')) {
            background = blackSquareGrey
        }

        $square.css('background', background)
    }

    function onDragStart(source, piece, position, orientation) {
        // do not pick up pieces if the game is over
        if (game.game_over()) return false;

        // only pick up pieces for the side to move
        if ((game.turn() === 'w' && piece.search(/^b/) !== -1) ||
            (game.turn() === 'b' && piece.search(/^w/) !== -1) ||
            (game.turn() === 'w' && team === 'Black') ||
            (game.turn() === 'b' && team === 'White')) {
            return false;
        }
    }

    function onDrop(source, target) {
        removeGreySquares()

        // See if the move is valid
        var move = game.move({
            from: source,
            to: target,
            promotion: 'q' // NOTE: always promote to a queen for example simplicity
        });

        // Move not valid
        if (move === null) return 'snapback';
    }

    // update the board position after the piece snap
    // for castling, en passant, pawn promotion
    function onSnapEnd() {
        board.position(game.fen());

        dotNetHelper.invokeMethodAsync('UpdateFen', game.fen());

        updateStatus();
    }

    function onMouseoverSquare(square, piece) {
        // only highlight pieces for the side to move
        if ((game.turn() === 'w' && team === 'White') ||
            (game.turn() === 'b' && team === 'Black')) {

            // get list of possible moves for this square
            var moves = game.moves({
                square: square,
                verbose: true
            })

            // exit if there are no moves available for this square
            if (moves.length === 0) return

            // highlight the square they moused over
            greySquare(square)

            // highlight the possible squares for this piece
            for (var i = 0; i < moves.length; i++) {
                greySquare(moves[i].to)
            }
        }
    }

    function onMouseoutSquare(square, piece) {
        removeGreySquares()
    }

    function updateStatus() {
        if (game) {
            dotNetHelper.invokeMethodAsync('UpdateStatus', {
                turn: game.turn(),
                check: game.in_check(),
                checkmate: game.in_checkmate(),
                draw: game.in_draw(),
                stalemate: game.in_stalemate()
            });
        }
    }

    var config = {
        draggable: true,
        onDragStart: onDragStart,
        onDrop: onDrop,
        onSnapEnd: onSnapEnd,
        onMouseoutSquare: onMouseoutSquare,
        onMouseoverSquare: onMouseoverSquare
    };

    board = ChessBoard('board', config);

    if (!position) {
        board.start();
    } else {
        board.position(position);
    }

    updateStatus();
}

