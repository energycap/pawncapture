﻿CREATE TABLE [dbo].[Move]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [fen] VARCHAR(250) NOT NULL, 
    [timestamp] DATETIMEOFFSET NOT NULL, 
    [player] INT NOT NULL, 
    [game] INT NOT NULL, 
    CONSTRAINT [FK_Move_Player] FOREIGN KEY ([player]) REFERENCES [player]([id]), 
    CONSTRAINT [FK_Move_Game] FOREIGN KEY ([game]) REFERENCES [game]([id])
)
