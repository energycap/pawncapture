﻿CREATE TABLE [dbo].[Player]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [team] INT NOT NULL, 
    [externalUserId] NVARCHAR(150) NOT NULL, 
    [name] VARCHAR(50) NULL, 
    CONSTRAINT [FK_Player_Team] FOREIGN KEY ([team]) REFERENCES [Team]([id])
)
